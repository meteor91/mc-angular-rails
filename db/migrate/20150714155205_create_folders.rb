class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.string :name, null: false
      t.integer :folder_id
      t.timestamps null: false
    end
    add_foreign_key :folders, :folders, on_delete: :cascade
  end
end
