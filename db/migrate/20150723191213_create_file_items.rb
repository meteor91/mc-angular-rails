class CreateFileItems < ActiveRecord::Migration
  def change
    create_table :file_items do |t|
      t.string :name, null: false
      t.string :type
      t.integer :folder_id, null: false
      t.timestamps null: false
    end
    add_foreign_key :file_items, :folders, on_delete: :cascade
  end
end
