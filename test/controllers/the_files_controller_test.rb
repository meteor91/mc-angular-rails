require 'test_helper'

class TheFilesControllerTest < ActionController::TestCase
  setup do
    @the_file = the_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:the_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create the_file" do
    assert_difference('TheFile.count') do
      post :create, the_file: {  }
    end

    assert_redirected_to the_file_path(assigns(:the_file))
  end

  test "should show the_file" do
    get :show, id: @the_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @the_file
    assert_response :success
  end

  test "should update the_file" do
    patch :update, id: @the_file, the_file: {  }
    assert_redirected_to the_file_path(assigns(:the_file))
  end

  test "should destroy the_file" do
    assert_difference('TheFile.count', -1) do
      delete :destroy, id: @the_file
    end

    assert_redirected_to the_files_path
  end
end
