
function isCharacterKeyPress(evt) {
    if (typeof evt.which == "undefined") {
        // This is IE, which only fires keypress events for printable keys
        return true;
    } else if (typeof evt.which == "number" && evt.which > 0) {
        // In other browsers except old versions of WebKit, evt.which is
        // only greater than zero if the keypress is a printable key.
        // We need to filter out backspace and ctrl/alt/meta key combinations
        return !evt.ctrlKey && !evt.metaKey && !evt.altKey || evt.keyCode == 13;
    }
    return false;
}



angular.module('mcController', [])
    .controller('KeyboardHandlerCtrl', ['$scope', 'PRESSED_KEY',
        function ($scope, PRESSED_KEY) {
            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == PRESSED_KEY.UP ||
                    $event.keyCode == PRESSED_KEY.DOWN ||
                    $event.keyCode == PRESSED_KEY.LEFT ||
                    $event.keyCode == PRESSED_KEY.RIGHT) {
                    $scope.$broadcast('files_manage', $event);
                } else if (isCharacterKeyPress($event)) {
                    $scope.$broadcast('terminal_input', $event);
                }
                /**/
            }
        }
    ]).controller('TerminalCtrl', ['$scope', '$rootScope', 'Files',
        function ($scope, $rootScope, Files) {
            $scope.input = '';
            $scope.currentDir = {id: 'root', name: 'root'};
            $rootScope.startListenKeyboard = function() {
                $rootScope.listenKeyboard = $scope.$on('terminal_input', function (event, data) {
                    if (data.keyCode == 8) {
                        if ($scope.input.length > 0)
                            $scope.input = $scope.input.substring(0, $scope.input.length - 1)
                    } else if (data.keyCode == 13) {
                        if ($scope.input.length != 0) {
                            var argv = $scope.input.split(' ');
                            var command = argv[0];

                            switch(command) {
                                case 'mkdir' : {
                                    if(typeof argv[1] != 'undefined') {
                                        Files.create({folder: {name: argv[1], folder_id: $scope.currentDir.id==-1?null:$scope.currentDir.id}}, function (response) {
                                                $scope.$broadcast('files_update', {action: 'create', item: response});
                                            }
                                        );
                                    }
                                    break;
                                }
                                case 'rm' : {
                                    if(typeof argv[1] != 'undefined') {
                                        /* experimental beta */
                                        var dsid =  $scope.currentDir;
                                        Files.delete({
                                                folder_id: $scope.currentDir.id,
                                                name: argv[1]
                                            }, function (response) {
                                                $scope.$broadcast('files_update', {action: 'delete', item: response});
                                            }
                                        );
                                    }
                                    break;
                                }
                            }
                        } else {
                            $scope.$parent.$broadcast('files_manage', data);
                        }
                        $scope.input = '';
                    } else {
                        $scope.input += data.key;
                    }
                });
            }
            $rootScope.startListenKeyboard();
            $rootScope.stopListenKeyboard = function() {
                $rootScope.listenKeyboard();
            }
        }
    ]).controller('FilesCtrl', ['$scope', '$rootScope', 'Files', 'PRESSED_KEY',
        function ($scope, $rootScope, Files, PRESSED_KEY) {

            $scope.dialogVisibility = true;
            $scope.showDialog = function () {
                $scope.dialogVisibility = !$scope.dialogVisibility;
            };

            $scope.selectedIndexLast = 0;
            $scope.key = 'undef';
            $scope.activePart = 1;

            function Tab(files, startIndex) {
                var self = this;
                this.index = {global: 0, local: startIndex};
                this.files = files;
                this.filesToShow = [];
                this.files.$promise.then(function(response) {
                    if(response.root.name != 'root')
                        self.files.list.unshift({name: '/..', id: response.root.folder_id});
                    for(var i=0; i<(response.list.length>15?15:response.list.length);i++)
                        self.filesToShow.push(response.list[i]);

                    self.currentDir = response.root;
                    $scope.$parent.currentDir = response.root;
                });
                this.selectedFile = function() {
                    return self.files.list[self.index.global];
                };
                this.up = function () {
                    if (self.index.local > 0) {
                        self.index.local -= 1;
                        self.index.global -= 1;
                    } else if ( self.index.global != 0) {
                        self.index.global -= 1;
                        self.filesToShow.pop();
                        self.filesToShow.unshift(self.files[self.index.global]);
                    }
                    $scope.selectedFile = self.files[self.index.global]
                    $scope.selectedIndex = self.index;
                };
                this.down = function () {
                    if (self.filesToShow.length - 1 > self.index.local) {
                        self.index.local += 1;
                    }
                    if(self.files.list.length - 1 > self.index.global ) {
                        self.index.global += 1;
                        if(self.index.global>self.index.local) {
                            self.filesToShow.shift();
                            self.filesToShow.push(self.files[self.index.global]);
                        }
                    }
                    $scope.selectedFile = self.files[self.index.global];
                    $scope.selectedIndex = self.index;
                };
                this.add = function(file) {
                    if (self.filesToShow.length<15) {
                        self.filesToShow.push(file);
                    }
                    self.files.list.push(file);
                }
            }
            $scope.leftTab = new Tab(Files.query({folder_id: -1}), 0);
            $scope.rightTab = new Tab(Files.query({folder_id: -1}), -1);
            $scope.getActiveTab = function() {
                return $scope.activePart == 1 ? $scope.leftTab : $scope.rightTab;
            };
            $scope.setActiveTab = function(tab) {
                if($scope.activePart == 1) {
                    $scope.leftTab = tab;
                } else {
                    $scope.rightTab = tab;
                }
            };

            $scope.$on('files_update', function(event, data) {
                if(data.action == 'create') {
                    $scope.getActiveTab().add(data.item);
                } else if(data.action == 'delete') {
                    $scope.setActiveTab(new Tab(Files.query({folder_id: $scope.getActiveTab().currentDir.id}), 0));
                }
            });
            $scope.$on('files_manage', function (event, data) {
                switch (data.keyCode) {
                    case PRESSED_KEY.ENTER : {
                        $scope.key = 'ENTER';
                        if( $scope.getActiveTab().selectedFile().name == '/..') {
                            var folder_id = $scope.getActiveTab().selectedFile().id === null ? -1 : $scope.getActiveTab().selectedFile().id;
                            $scope.setActiveTab(new Tab(Files.query({folder_id: folder_id}), 0));
                        } else {
                            $scope.setActiveTab(new Tab(Files.query({folder_id: $scope.getActiveTab().selectedFile().id}), 0));

                        }
                        break;
                    }
                    case PRESSED_KEY.UP : {
                        $scope.key = 'UP';
                        $scope.getActiveTab().up();
                        break;
                    }
                    case PRESSED_KEY.DOWN : {
                        $scope.key = 'DOWN';
                        $scope.getActiveTab().down();
                        break;
                    }
                    case PRESSED_KEY.LEFT : {
                        $scope.key = 'LEFT';
                        if ($scope.activePart == 2) {
                            $scope.activePart = 1;
                            $scope.leftTab.index.local = $scope.selectedIndexLast;
                            $scope.selectedIndexLast = $scope.rightTab.index.local;
                            $scope.rightTab.index.local = -1;
                        }
                        break;
                    }
                    case PRESSED_KEY.RIGHT : {
                        $scope.key = 'RIGHT';
                        if ($scope.activePart == 1) {
                            $scope.activePart = 2;
                            $scope.rightTab.index.local = $scope.selectedIndexLast;
                            $scope.selectedIndexLast = $scope.leftTab.index.local;
                            $scope.leftTab.index.local = -1;
                        }
                        break;
                    }
                    default : {
                        $scope.key = 'unsupported';
                    }
                }
            });
        }
    ]);