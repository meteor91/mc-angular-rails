//= require jquery
//= require angular/angular
//= require angular-resource/angular-resource

//= require angular-rails-templates
//= require_tree ./templates

//= require app
//= require controllers