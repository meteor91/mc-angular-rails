
angular.module('mcApp', [
    'mcServices',
    'mcController'
]);

angular.module('mcServices', ['ngResource', 'templates']).factory('Files', ['$resource',
    function ($resource) {
        return $resource('/folders/:id.json', {}, {
            query: { method: 'GET', url: '/folders/from_folder/:folder_id.json', params: {folder_id: '@folder_id'}, isArray: false },
            delete: { method: 'DELETE', url: '/folders/delete/:folder_id/:name.json', params: {folder_id: '@folder_id', name: '@name'} },
            create: { method: 'POST' }
        });
    }
]).config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] =
        angular.element('meta[name="csrf-token"]').attr('content');
}).constant('PRESSED_KEY', {
    'ENTER': 13,
    'UP': 38,
    'DOWN': 40,
    'LEFT': 37,
    'RIGHT': 39
}).filter('range', function(){
    return function(n) {
        var res = [];
        for (var i = 0; i < n; i++) {
            res.push(i);
        }
        return res;
    };
}).directive('createButton', function() {
    return {
        restrict: 'E',
        scope: {
            'close': '&onClose',
            'visible': '=isVisible'
        },
        templateUrl: 'create-button.html'
    }
}).directive('addItemDialog', function() {
    return {
        restrict: 'E',
        templateUrl: 'add-item-dialog.html',
        link: function(scope, element, attrs) {
            scope.dialogVisibility = true;
            scope.item="gfdgf";
            scope.showDialog = function () {
                scope.dialogVisibility = false;
                scope.$root.stopListenKeyboard();
            };
            scope.close = function() {
                scope.item = "";
                scope.dialogVisibility = true;
                scope.$root.startListenKeyboard();
            }
            scope.add = function() {
                if (scope.item != "")
                    scope.$emit('add_item', scope.item);
                scope.close();
            };
        }
    }
});