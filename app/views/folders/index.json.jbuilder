json.array!(@folders) do |folder|
  json.extract! folder, :id, :name, :created_at, :updated_at
  json.url folder_url(folder, format: :json)
end
