json.root do
  json.id @root.id
  json.name @root.name
  json.folder_id @root.folder_id
end
json.list @folders, :id, :name, :created_at, :updated_at, :folder_id
#json.child! do
#  json.id @root.id
#  json.name @root.name
#end
#json.array!(@folders) do |folder|
#  json.extract! folder, :id, :name, :created_at, :updated_at, :folder_id
#end