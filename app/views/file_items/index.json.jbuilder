json.array!(@file_items) do |file_item|
  json.extract! file_item, :id
  json.url file_item_url(file_item, format: :json)
end
