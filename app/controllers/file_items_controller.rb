class FileItemsController < ApplicationController
  before_action :set_file_item, only: [:show, :edit, :update, :destroy]

  # GET /file_items
  # GET /file_items.json
  def index
    @file_items = FileItem.all
  end

  def from_folder
    @file_items = FileItem.where(folder_id: params[:id])
  end

  # GET /file_items/1
  # GET /file_items/1.json
  def show
  end

  # GET /file_items/new
  def new
    @file_item = FileItem.new
  end

  # GET /file_items/1/edit
  def edit
  end

  # POST /file_items
  # POST /file_items.json
  def create
    @file_item = FileItem.new(file_item_params)

    respond_to do |format|
      if @file_item.save
        format.html { redirect_to @file_item, notice: 'File item was successfully created.' }
        format.json { render :show, status: :created, location: @file_item }
      else
        format.html { render :new }
        format.json { render json: @file_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /file_items/1
  # PATCH/PUT /file_items/1.json
  def update
    respond_to do |format|
      if @file_item.update(file_item_params)
        format.html { redirect_to @file_item, notice: 'File item was successfully updated.' }
        format.json { render :show, status: :ok, location: @file_item }
      else
        format.html { render :edit }
        format.json { render json: @file_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /file_items/1
  # DELETE /file_items/1.json
  def destroy
    @file_item.destroy
    respond_to do |format|
      format.html { redirect_to file_items_url, notice: 'File item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_file_item
      @file_item = FileItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def file_item_params
      params.require(:file_item).permit(:name, :folder_id)
    end
end
