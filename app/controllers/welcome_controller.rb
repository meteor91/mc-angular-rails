class WelcomeController < ApplicationController
  def index
    render :layout => false
  end

  def index_dev
    render :layout => false
  end

  def features
    render :layout => false
  end
end
