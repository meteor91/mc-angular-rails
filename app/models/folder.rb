class Folder < ActiveRecord::Base
  has_many :folder
  has_many :file_items
  belongs_to :folder
  validates_uniqueness_of :name, scope: :folder_id
end
